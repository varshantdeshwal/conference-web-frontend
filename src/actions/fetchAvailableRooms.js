var result = async function getCall(id, capacity) {
  const res = await fetch(
    "http://17.196.29.186:3042/rooms/getRoomsWithCapacity/" + id + "/" + capacity,

    {
      method: "GET"
    }
  );

  const data = await res.json();

  return data;
};

export default result;
