var result = async function insertBookCall(data) {
  const res = await fetch(
    "http://17.196.29.186:3042/bookings/",

    {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      },
      body: data
    }
  );
  const booking = await res.json();

  return booking;
};

export default result;
