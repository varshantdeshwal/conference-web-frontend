import React from "react";
export const populateDevcenters = data => {
  let items = [];
  for (let i = 0; i < data.length; i++) {
    items.push(<option key={i}>{data[i].name}</option>);
  }
  return items;
};
export const populateClients = data => {
  let items = [];
  for (let i = 0; i < data.length; i++) {
    items.push(<option key={i}>{data[i].name}</option>);
  }
  return items;
};
export const populateLocations = data => {
  let items = [];
  for (let i = 0; i < data.length; i++) {
    items.push(<option key={i}>{data[i].location}</option>);
  }
  return items;
};
export const populateSeatingCapacity = data => {
  let items = [];
  for (let i = 0; i < data.length; i++) {
    items.push(<option key={i}>{data[i]}</option>);
  }
  return items;
};
