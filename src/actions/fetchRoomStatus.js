var result = async function getClientsCall(id, date) {
  const res = await fetch(
    "http://17.196.29.186:3042/bookings/" + id + "/" + date,

    {
      method: "GET"
    }
  );
  const data = await res.json();

  return data;
};

export default result;
