export const onDropdownSelected = (data, name) => {
  return {
    type: "ON_DROPDOWN_SELECTED",
    data,
    name
  };
};
export const onLoadRequest = data => {
  return {
    type: "ON_LOAD",
    data
  };
};
export const onSearch = data => {
  return {
    type: "ON_SEARCH",
    data
  };
};
export const onSearchFieldChange = () => {
  return { type: "ON_SEARCHFIELD_CHANGE" };
};
export const captureDate = date => {
  return { type: "CAPTURE_DATE", date };
};
export const onBooked = notificationMsg => {
  return { type: "ON_BOOKED", notificationMsg };
};
