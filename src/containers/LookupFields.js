import React, { Component } from "react";

import {
  onDropdownSelected,
  onLoadRequest,
  onSearch,
  onSearchFieldChange,
  captureDate
} from "../actions/index";
import fetchDevcenters from "../actions/fetchDevcenters";
import fetchClients from "../actions/fetchClients";
import fetchLocations from "../actions/fetchLocations";
import fetchSeatingCapacity from "../actions/fetchSeatingCapacity";
import fetchAvailableRooms from "../actions/fetchAvailableRooms";
import fetchRoomStatus from "../actions/fetchRoomStatus";

import { bindActionCreators } from "redux";
import SearchButton from "../components/searchButton";
import {
  populateDevcenters,
  populateClients,
  populateLocations,
  populateSeatingCapacity
} from "../actions/populateDropdown";
import DateComponent from "../components/dateComponent";
import { connect } from "react-redux";
import DropdownComponent from "../components/dropdown";
import Spinner from "../components/Spinner";

class LookupFields extends Component {
  state={
    spinner:false
  }
  componentDidMount() {
    fetchDevcenters().then(data => {
      this.props.onLoadRequest(data);
    });
  }
  onSelected(id, name) {
    switch (name) {
      case "client":
        fetchClients(id).then(data => {
          this.props.onDropdownSelected(data, name);
        });
        this.props.onSearchFieldChange();
        break;

      case "location":
        fetchLocations(id).then(data => {
          this.props.onDropdownSelected(data, name);
        });
        this.props.onSearchFieldChange();
        break;
      case "seatingcap":
        fetchSeatingCapacity(id).then(data => {
          this.props.onDropdownSelected(data, name);
        });
        this.props.onSearchFieldChange();
        break;
      case "nothing":
        this.props.onSearchFieldChange();
        break;
      default:
    }
  }
 async search(id, capacity) {
   
    await this.props.onSearchFieldChange();
    fetchAvailableRooms(id, capacity).then(data =>
      data.map(room =>
        fetchRoomStatus(room._id, this.props.date.format("YYYY-MM-DD")).then(
          slots => {
            var details = {
              _id: room._id,
              roomNumber: room.roomNumber,
              seatingCpacity: room.seatingCapacity,
              bookedSlots: slots
            };
            this.props.onSearch(details);
            this.setState({spinner:false});
          }
        )
      )
    );
  }
  captureDate(e) {
    this.props.onSearchFieldChange();
    this.props.captureDate(e);
  }
  render() {
  
    return (
      <div>
      <div className="timeline">
        <span style={{ fontSize: 30, margin: 20 }}>
          Conference Room Booking
        </span>
        <hr />
        <table className="inputTable">
          <tbody>
            <tr>
              <td className="search">Development Center</td>
              <td>
                <DropdownComponent
                  name="Development Center"
                  changeEvent={e =>
                    this.onSelected(
                      this.props.devcenters[e.target.selectedIndex - 1]._id,
                      "client"
                    )
                  }
                  items={populateDevcenters(this.props.devcenters)}
                />
              </td>
            </tr>
            <tr>
              <td className="search">Client</td>
              <td>
                <DropdownComponent
                  name="Client"
                  changeEvent={e =>
                    this.onSelected(
                      this.props.clients[e.target.selectedIndex - 1]._id,
                      "location"
                    )
                  }
                  items={populateClients(this.props.clients)}
                />
              </td>
            </tr>
            <tr>
              <td className="search">Location</td>
              <td>
                <DropdownComponent
                  name="Location"
                  changeEvent={e =>
                    this.onSelected(
                      this.props.locations[e.target.selectedIndex - 1]._id,
                      "seatingcap"
                    )
                  }
                  items={populateLocations(this.props.locations)}
                />
              </td>
            </tr>
            <tr>
              <td className="search">Seating Capacity</td>

              <td>
                <DropdownComponent
                  name="Seating Capacity"
                  changeEvent={e =>
                    this.onSelected(
                      this.props.seatingcap[e.target.selectedIndex - 1]._id,
                      "nothing"
                    )
                  }
                  items={populateSeatingCapacity(this.props.seatingcap)}
                />
              </td>
            </tr>
            <tr>
              <td className="search">Date</td>
              <td>
                <DateComponent changeEvent={date => this.captureDate(date)} />
              </td>
            </tr>
          </tbody>
        </table>
        <br />

        <SearchButton
          searchEvent={() => {
            if (
              
              document.getElementById("Seating Capacity").selectedIndex === 0||
              this.props.date === null
            ) {
              alert("Input Missing");
            } else {
              this.setState({spinner:true});
              this.search(
                this.props.locations[
                  document.getElementById("Location").selectedIndex - 1
                ]._id,
                this.props.seatingcap[
                  document.getElementById("Seating Capacity").selectedIndex - 1
                ]
              );
            }
          }}
        />
      </div>
      {this.state.spinner?<Spinner></Spinner>:null}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    devcenters: state.fields.devcenters,
    clients: state.fields.clients,
    locations: state.fields.locations,
    seatingcap: state.fields.seatingcap,
    // availableRooms: state.rooms,
    date: state.date
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      onDropdownSelected: onDropdownSelected,
      onLoadRequest: onLoadRequest,
      onSearch: onSearch,
      onSearchFieldChange: onSearchFieldChange,
      captureDate: captureDate
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LookupFields);
