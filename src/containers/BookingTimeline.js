import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import addBooking from "../actions/addBooking";
import Thead from "../components/thead";

import { onBooked } from "../actions/index";
import "react-notifications/lib/notifications.css";
import Spinner from "../components/Spinner";

class BookingTimeline extends Component {
  state = {
    temp_book: [],
    booked_slots: [],
    spinner:false
  };
  componentWillReceiveProps(newProps) {
    this.setState({ temp_book: this.props.reset });
  }

  render() {
 
    var columnNames = [
      "",

      "8-9",

      "9-10",

      "10-11",

      "11-12",

      "2-3",

      "3-4",

      "4-5",

      "5-6",

      "6-7"
    ];

    var booked_slots = [];

    for (let i = 1; i <= this.props.availableRooms.length; i++) {
      for (
        let j = 0;
        j < this.props.availableRooms[i - 1].bookedSlots.length;
        j++
      ) {
        booked_slots = [
          ...booked_slots,

          i * 10 + this.props.availableRooms[i - 1].bookedSlots[j]
        ];
      }
    }

    return (
    
      <div className="timeline">
        <table className="bookTable" style={{ width: "700px" }}>
          {this.props.availableRooms.length === 0 ? null : <Thead />}
          <tbody>
            {this.props.availableRooms.map((element, row_index) => {
              return (
                <tr key={row_index}>
                  {columnNames.map((slot, column_index) => {
                    let cell_index = (row_index + 1) * 10 + column_index;
                    if (slot === "")
                      return (
                        <td key={column_index} style={{ textAlign: "center" }}>
                          <b> Room: {element.roomNumber}</b>
                        </td>
                      );
                    else
                      return booked_slots.includes(cell_index) ? (
                        <td
                          key={cell_index}
                          id={cell_index}
                          className="border-hover"
                          style={{
                            backgroundColor: "rgb(179, 179, 179)",
                            borderColor: "black",
                            border: "solid",
                            borderWidth: 0.7
                          }}
                          disabled="disabled"
                        />
                      ) : (
                        <td
                          key={cell_index}
                          className="available"
                          id={cell_index}
                          onClick={() =>
                            this.handleClick(
                              element._id,
                              column_index,
                              cell_index
                            )
                          }
                        />
                      );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>

        {this.props.availableRooms.length === 0 ? (
          <span />
        ) : (
          <button
            className="btn btn-success"
            onClick={() => this.book()}
            value="Book"
          >
            Book
          </button>
        )}
        {this.state.spinner?<Spinner></Spinner>:null}
      </div>
    );
  }

  handleClick = (room, slot, id) => {
    var flag = false;
    var colorFlag = false;
    var temp_book = [...this.state.temp_book];
    for (let i = 0; i < temp_book.length; i++) {
      if (temp_book[i].roomId === room) {
        if (temp_book[i].slots.includes(slot)) {
          var index = temp_book[i].slots.indexOf(slot);
          temp_book[i].slots.splice(index, 1);
          colorFlag = true;
        }
        if (temp_book[i].slots.length === 0) {
          temp_book.splice(i, 1);
        }
      }
    }
    if (colorFlag === false) {
      for (let i = 0; i < temp_book.length; i++) {
        if (temp_book[i].roomId === room) {
          temp_book[i].slots = [...temp_book[i].slots, slot];
          flag = true;
        }
      }
      if (flag === false) {
        temp_book = [
          ...this.state.temp_book,
          { roomId: room, slots: [slot], date: this.props.date, id: id }
        ];
      }
      document.getElementById(id).style.background = "rgb(96, 189, 153)";
    }
    if (colorFlag === true) {
      document.getElementById(id).style.background = "white";
    }
    this.setState({ temp_book });
  };

  async book() {

    if (this.state.temp_book.length === 0) {
      alert("Nothing Selected");
    } else {
      this.setState({spinner:true});
      var temp_book = [...this.state.temp_book];
      var allRooms = [];

      for (let i = 0; i < temp_book.length; i++) {
      allRooms.push(temp_book[i]);
    }
    for await (let room of allRooms){
      let booking = {
        roomId: room.roomId,
        slots: room.slots,
        date: room.date.format("YYYY-MM-DD")
      };
       addBooking(JSON.stringify(booking)).then(result => {
        if (result.message === "booking done") {
          
        } else {
          this.props.onBooked("error");
          this.setState({spinner:false});
         
        }
      });
    }
      // allRooms.map(room => {
      //   let booking = {
      //     roomId: room.roomId,
      //     slots: room.slots,
      //     date: room.date.format("YYYY-MM-DD")
      //   };
      //   addBooking(JSON.stringify(booking)).then(result => {
      //     if (result.message === "booking done") {
            
      //     } else {
      //       this.props.onBooked("error");
      //       this.setState({spinner:false});
           
      //     }
      //   });
      // })
      this.props.onBooked("success");
      this.props.onBooked("");
      for (let i = 0; i < temp_book.length; i++) {
        const row_id = Math.floor(temp_book[i].id/10);
        for (let j=0; j < temp_book[i].slots.length; j++){
        document.getElementById(row_id*10 + temp_book[i].slots[j]).style.background =
          "rgb(179, 179, 179)";
      }
    }
      temp_book = [];
      this.setState({ temp_book });
            this.setState({spinner:false});
    }
  }
}
function mapStateToProps(state) {
  return {
    availableRooms: state.rooms,
    date: state.date,
    reset: state.resetTimeline,
    notificationMsg:state.notificationMsg
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ onBooked: onBooked}, dispatch);
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingTimeline);
