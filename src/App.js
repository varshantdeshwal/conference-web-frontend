import React, { Component } from "react";
import LookupFields from "./containers/LookupFields";
import BookingTimeline from "./containers/BookingTimeline";
import { connect } from "react-redux";

import MessageComponent from "./components/MessageComponent";

class App extends Component {
  render() {
  
    return (
      <div>
        <LookupFields />
        <BookingTimeline />
        <MessageComponent message={this.props.notificationMsg} />
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    notificationMsg: state.notificationMsg
  };
}

export default connect(
  mapStateToProps,
  null
)(App);
