import React, { Component } from "react";

class DropdownComponent extends Component {
  render() {
    return (
      <select
        id={this.props.name}
        onChange={this.props.changeEvent}
        style={{ width: 192, height: 30 }}
        className="form-control drop userinput"
      >
        <option disabled value>
          {" "}
          select an option{" "}
        </option>

        {this.props.items}
      </select>
    );
  }
}

export default DropdownComponent;
