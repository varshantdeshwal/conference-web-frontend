import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker-cssmodules.css";
import { connect } from "react-redux";
import moment from "moment";
class DateComponent extends Component {
  handleDateChangeRaw = e => {
    e.preventDefault();
  };
  render() {
    return (
      <DatePicker
        className="dateselect"
        onChange={this.props.changeEvent}
        selected={this.props.date}
        placeholderText="Click to select a date"
        dateFormat="DD/MM/YYYY"
        minDate={moment()}
        maxDate={moment().add(5, "days")}
        onChangeRaw={this.handleDateChangeRaw}
      />
    );
  }
}
function mapStateToProps(state) {
  return {
    date: state.date
  };
}

export default connect(
  mapStateToProps,
  null
)(DateComponent);
