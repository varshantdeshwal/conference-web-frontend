import React, { Component } from "react";
import "react-notifications/lib/notifications.css";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
class MessageComponent extends Component {
  createNotification = type => {
    if (type === "success") {
      NotificationManager.success("Booking Successful", "Successful");
    } else if (type === "error") {
      NotificationManager.info("Already Booked");
    }
  };

  render() {
    this.createNotification(this.props.message);
    return <NotificationContainer />;
  }
}

export default MessageComponent;
