import React, { Component } from "react";
class SearchButton extends Component {
  state = {};
  render() {
    return (
      <input
        className="btn btn-info"
        type="submit"
        onClick={this.props.searchEvent}
        value="Find"
      />
    );
  }
}

export default SearchButton;
