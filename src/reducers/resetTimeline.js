const resetTimeline = (state = [], action) => {
  switch (action.type) {
    case "ON_SEARCHFIELD_CHANGE":
      return [];

    default:
      return state;
  }
};
export default resetTimeline;
