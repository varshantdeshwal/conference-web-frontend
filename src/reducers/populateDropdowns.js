const initialState = {
  devcenters: [],
  clients: [],
  locations: [],
  seatingcap: []
};

const fields = (state = initialState, action) => {
  switch (action.type) {
    case "ON_DROPDOWN_SELECTED":
      switch (action.name) {
        case "client":
          document.getElementById("Client").selectedIndex = 0;
          return {
            devcenters: state.devcenters,
            clients: action.data,
            locations: [],
            seatingcap: []
          };

        case "location":
          document.getElementById("Location").selectedIndex = 0;
          return {
            devcenters: state.devcenters,
            clients: state.clients,
            locations: action.data,
            seatingcap: []
          };

        case "seatingcap":
          document.getElementById("Seating Capacity").selectedIndex = 0;
          return {
            devcenters: state.devcenters,
            clients: state.clients,
            locations: state.locations,
            seatingcap: action.data
          };

        default:
          return state;
      }

    case "ON_LOAD":
      return {
        devcenters: action.data,
        clients: state.clients,
        locations: state.locations,
        seatingcap: state.seatingcap
      };

    default:
      return state;
  }
};
export default fields;
