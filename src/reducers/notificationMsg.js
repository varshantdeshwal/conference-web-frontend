const notificationMsg = (state = "", action) => {
  switch (action.type) {
    case "ON_BOOKED":
      return action.notificationMsg;
    case "ON_SEARCHFIELD_CHANGE":
      return "";

    default:
      return state;
  }
};
export default notificationMsg;
