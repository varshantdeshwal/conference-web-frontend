import { combineReducers } from "redux";
import fields from "./populateDropdowns";
import rooms from "./availableRooms";
import date from "./captureDate";
import resetTimeline from "./resetTimeline";
import notificationMsg from "./notificationMsg";
export default combineReducers({
  fields,
  rooms,
  date,
  resetTimeline,
  notificationMsg
});
