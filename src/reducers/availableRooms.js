const availableRooms = (state = [], action) => {
  switch (action.type) {
    case "ON_SEARCH":
      return [...state, action.data];
    case "ON_SEARCHFIELD_CHANGE":
      return [];

    default:
      return state;
  }
};
export default availableRooms;
