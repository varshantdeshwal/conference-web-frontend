**Its a conference room booking web application.**

**Demo** - https://vimeo.com/300489087


**Tools and Technologies used :**

    - React JS
    - Redux JS
    - ES6
    - JavaScript
    - CSS
    - HTML
	

**Local Setup -**

  1. Clone the repository.
  3. Run command "npm install" inside project root directory to install dependencies.
  4. Run command "npm start" to run the application.

**NOTE** - Change the base url to ```http://localhost:<port>/``` in each api call

For any help, drop a mail to **varshant.14@gmail.com**